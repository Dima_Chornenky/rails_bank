
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails_bank/version'

Gem::Specification.new do |spec|
  spec.name          = 'rails_bank'
  spec.version       = RailsBank::VERSION
  spec.authors       = ['dima-chornenky']
  spec.email         = ['dima.chornenky@gmail.com']

  spec.summary       = 'Ruby client library for the Railsbank API'
  spec.description   = spec.summary
  spec.homepage      = 'https://bitbucket.org/Dima_Chornenky/rails_bank/src'
  spec.license       = 'MIT'

  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.42.0'
end
