# RailsBank

A thin wrapper for Railsbank's API.
This gem supports v1 of the Railsbank API. Refer to Railsbank's API documentation for details of the expected requests and responses.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rails_bank'
```

The gem is compatible with Ruby 2.5.1 and onwards.

## Configuration

There are 5 configuration options:

```ruby
RailsBank.configure do |config|
  config.api_key      = 'MY_API_KEY'
  config.open_timeout = 30
  config.read_timeout = 80
  config.api_version  = 'v1'
  config.environment  = 'play'
end
```
## Usage

You can make API calls by using an instance of the `API` class:

```ruby
api = RailsBank::API.new
```

Alternatively, you can set an API key here instead of in the initializer:

```ruby
api = RailsBank::API.new(api_key: 'API_KEY')
```

### Resources

All resources share the same interface when making API calls. Use `.create` to create a resource, `.retrieve` to find one, and `.all` to fetch all resources.

**Note:** *All param keys should be a symbol*

#### Endusers

Endusers are typically people and/or companies that are consumers of the products/services of Railsbank Customers.

```ruby
api.enduser.create(params)   # => Add new enduser.
api.enduser.wait(id)         # => Waits until enduser will be in final state
api.enduser.retrieve(id)     # => Find an enduser
```


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/Dima_Chornenky/rails_bank. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the RailsBank project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/Dima_Chornenky/rails_bank/master/CODE_OF_CONDUCT.md).
