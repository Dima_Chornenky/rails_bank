require 'rack'
require 'rest-client'

require 'rails_bank/version'
require 'rails_bank/api'
require 'rails_bank/configure'
require 'rails_bank/recource'
require 'rails_bank/recources/enduser'

require 'rails_bank/errors/connection_error'
require 'rails_bank/errors/rails_bank_error'
require 'rails_bank/errors/request_error'
require 'rails_bank/errors/server_error'

module RailsBank
  extend Configuration
end
