module RailsBank
  module Configuration
    attr_accessor :api_key, :open_timeout, :read_timeout, :api_version, :environment

    def self.extended(base)
      base.reset
    end

    def configure
      yield self
    end

    def reset
      self.api_key      = nil
      self.open_timeout = 30
      self.read_timeout = 80
      self.api_version  = 'v1'
      self.environment  = 'play'
    end

    def endpoint
      "https://#{environment}.railsbank.com/#{api_version}/customer/"
    end
  end
end
