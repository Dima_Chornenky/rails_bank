module RailsBank
  class Beneficiary < Resource
    def create(params)
      post(url: url_for('beneficiaries'), params: params)
    end

    def wait(id)
      get(url: url_for("beneficiaries/#{id}/wait"), params: {})
    end

    def retrieve(id)
      get(url: url_for("beneficiaries/#{id}"), params: {})
    end
  end
end
