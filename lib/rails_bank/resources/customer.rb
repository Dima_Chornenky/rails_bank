module RailsBank
  class Customer < Resource
    def me
      get(url: url_for('me'), params: {})
    end

    def partners
      get(url: url_for('my/partners'), params: {})
    end

    def products
      get(url: url_for('my/products'), params: {})
    end
  end
end
