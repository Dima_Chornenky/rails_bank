module RailsBank
  class Webhook < Resource
    def information
      get(url: url_for('webhook'), params: {})
    end

    def retrieve
      get(url: url_for('notifications'), params: {})
    end

    def create(params)
      post(url: url_for('webhook'), params: params)
    end

    def clean
      delete(url: url_for('webhook'), params: {})
    end
  end
end
