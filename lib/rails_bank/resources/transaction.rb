module RailsBank
  class Transaction < Resource
    def create(params)
      post(url: url_for('transactions'), params: params)
    end

    def fx_create(params)
      post(url: url_for('transactions/fx'), params: params)
    end

    def retrieve(id)
      get(url: url_for("transactions/#{id}"), params: {})
    end

    def list(params)
      get(url: url_for('transactions'), params: params)
    end

    def quote(params)
      put(url: url_for('transactions/fx/quote'), params: params)
    end
  end
end
