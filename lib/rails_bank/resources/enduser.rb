module RailsBank
  class Enduser < Resource
    def create(params)
      post(url: url_for('endusers'), params: params)
    end

    def wait(id)
      get(url: url_for("endusers/#{id}/wait"), params: {})
    end

    def retrieve(id)
      get(url: url_for("endusers/#{id}"), params: {})
    end
  end
end
