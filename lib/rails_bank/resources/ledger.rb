module RailsBank
  class Ledger < Resource
    def create(params)
      post(url: url_for('ledgers'), params: params)
    end

    def retrieve(id)
      get(url: url_for("ledgers/#{id}"), params: {})
    end

    def assign_iban(id)
      post(url: url_for("ledgers/#{id}/assign-iban"), params: {})
    end

    def wait(id)
      get(url: url_for("ledgers/#{id}/wait"), params: {})
    end
  end
end
